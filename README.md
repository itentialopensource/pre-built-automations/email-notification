<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_

This Pre-Built Automation has been deprecated as of 02-21-2024 and will be end of life on 02-21-2025. The capabilities of this Pre-Built Automation have been replaced by the Pre-Built [Email - SMTP](https://gitlab.com/itentialopensource/pre-built-automations/email-smtp).

<!-- Update the below line with your Pre-Built name -->
# Email Notification

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [Email Notification](#email-notification)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Operations Manager and JSON-Form](#operations-manager-and-json-form)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Requirements](#requirements)
  - [Features](#features)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
    - [Input Variables](#input-variables)
  - [Additional Information](#additional-information)


## Overview

This Pre-Built integrates with the Email Adapter to deliver email notifications to end users.  This Pre-Built simplifies the process of sending an email at any point in an automation.

## Operations Manager and JSON-Form

This Pre-Built has an Operations Manager automation `Email Notification` that calls a workflow. The automation uses a JSON-Form to specify common fields populated when an issue is created. The workflow the automation calls queries data from the formData job variable.


## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`

## Requirements

This Pre-Built requires the following:

- The [Email Adapter](https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-email) is configured and integrated with an email server

## Features

The main benefits and features of the Pre-Built are outlined below:

* Allows a user to send a notification via email


## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the Requirements section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager automation `Email Notification` or run the workflow Email Notification in a childJob task.

### Input Variables
_Example_

```json
{
  "senderEmailAddress": "The email sender",
  "receiverEmailAddress": "The email receiver",
  "subject": "The email subject",
  "body": "The email body"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
