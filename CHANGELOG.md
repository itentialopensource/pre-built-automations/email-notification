
## 0.0.18 [02-23-2024]

* Updates package.json description

See merge request itentialopensource/pre-built-automations/email-notification!18

---

## 0.0.17 [02-21-2024]

* Adds deprecation notice

See merge request itentialopensource/pre-built-automations/email-notification!15

---

## 0.0.16 [07-20-2023]

* Update to use Open Source adapter-email as well as updates README

See merge request itentialopensource/pre-built-automations/email-notification!14

---

## 0.0.15 [06-20-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/email-notification!7

---

## 0.0.14 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/email-notification!6

---

## 0.0.13 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/email-notification!5

---

## 0.0.12 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/email-notification!4

---

## 0.0.11 [05-17-2021]

* Bug fixes and performance improvements

See commit a823109

---

## 0.0.10 [05-05-2021]

* Bug fixes and performance improvements

See commit 1149724

---

## 0.0.9 [03-23-2021]

* Bug fixes and performance improvements

See commit f20ec4c

---

## 0.0.8 [02-27-2021]

* Bug fixes and performance improvements

See commit 8a965f2

---

## 0.0.7 [02-27-2021]

* patch/2021-02-27T14-19-14

See merge request itential/sales-engineer/selabprebuilts/email-notification!7

---

## 0.0.6 [02-25-2021]

* Jerry.itential master patch 23172

See merge request itential/sales-engineer/selabdemos/notification-email!6

---

## 0.0.5 [02-24-2021]

* patch/2021-01-28T10-44-31

See merge request itential/sales-engineer/selabdemos/notification-email!1

---

## 0.0.4 [02-22-2021]

* patch/2021-01-28T10-44-31

See merge request itential/sales-engineer/selabdemos/notification-email!1

---

## 0.0.3 [01-28-2021]

* patch/2021-01-28T10-44-31

See merge request itential/sales-engineer/selabdemos/notification-email!1

---

## 0.0.2 [01-28-2021]

* Bug fixes and performance improvements

See commit ec6e9c4

---\n\n\n
