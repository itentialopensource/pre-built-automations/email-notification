import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';
const EmailNotificationWrapperJob0Data = require('../fixtures/stubs/Email Notification Wrapper Job0.json');
const EmailNotificationWrapperJob1Data = require('../fixtures/stubs/Email Notification Wrapper Job1.json');
const EmailNotificationJob2Data = require('../fixtures/stubs/Email Notification Job2.json');
const EmailNotificationJob3Data = require('../fixtures/stubs/Email Notification Job3.json');

function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });
    // Check if Stub flag is enabled
    if(isStub){
      stubTasks.forEach(stubTask=>{
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }
    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();

  return workflowRunner;
}

// Function to delete the stubbed workflow and reimport it without the stub tasks
function replaceStubTasks(workflowRunner, workflowName) {
    workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: workflowName });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
}

describe('Default: Cypress Tests', function () {
  let prebuiltRunner;
  let EmailNotificationWrapperJob0Workflow;
  let EmailNotificationWrapperJob1Workflow;
  let EmailNotificationJob2Workflow;
  let EmailNotificationJob3Workflow;
  
  before(function () {
    //creates a prebuilt runner for importing the prebuilt
    cy.fixture(`../../../artifact.json`).then((data) => {
      prebuiltRunner = new PrebuiltRunner(data);
    });
    cy.fixture(`../../../bundles/workflows/Email Notification Wrapper.json`).then((data) => {
      EmailNotificationWrapperJob0Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Email Notification Wrapper.json`).then((data) => {
      EmailNotificationWrapperJob1Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Email Notification.json`).then((data) => {
      EmailNotificationJob2Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Email Notification.json`).then((data) => {
      EmailNotificationJob3Workflow = data;
    });
    
  });

  describe('Default: Imports Pre-Built', function () {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    it('Default: Should import the prebuilt into IAP', function () {
        prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
        prebuiltRunner.importPrebuilt.single({});
    });
  })

  describe('Email Notification Wrapper', function() {
    it('It should run success path for Email Notification', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(EmailNotificationWrapperJob0Workflow, importWorkflow, isStub, EmailNotificationWrapperJob0Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: EmailNotificationWrapperJob0Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(EmailNotificationWrapperJob0Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(EmailNotificationWrapperJob0Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, EmailNotificationWrapperJob0Workflow);
      });
    })
  })

  describe('Email Notification Wrapper', function() {
    it('It should run failure path for Email Notification', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(EmailNotificationWrapperJob1Workflow, importWorkflow, isStub, EmailNotificationWrapperJob1Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: EmailNotificationWrapperJob1Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(EmailNotificationWrapperJob1Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(EmailNotificationWrapperJob1Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, EmailNotificationWrapperJob1Workflow);
      });
    })
  })

  describe('Email Notification', function() {
    it('Email Notification: It should run success path for Email Notification', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(EmailNotificationJob2Workflow, importWorkflow, isStub, EmailNotificationJob2Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: EmailNotificationJob2Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(EmailNotificationJob2Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(EmailNotificationJob2Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, EmailNotificationJob2Workflow);
      });
    })
  })
});